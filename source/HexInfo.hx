package;

class HexInfo {

    public var img (default, null): String;

    public function new(img: String) {
        this.img = img;
    }
}
