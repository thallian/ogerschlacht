package;

class FractionalHex {

    public var q(default, null): Float;
    public var r(default, null): Float;
    public var s(default, null): Float;

    public function new(q: Float, r: Float, ?s: Float) {
        this.q = q;
        this.r = r;
        this.s = s == null ? (-q -r) : s;
    }

    public function to_hex(): Hex {
        var q: Int = Math.round(this.q);
        var r: Int = Math.round(this.r);
        var s: Int = Math.round(this.s);

        var q_diff: Float = Math.abs(q - this.q);
        var r_diff: Float = Math.abs(r - this.r);
        var s_diff: Float = Math.abs(s - this.s);

        if (q_diff > r_diff && q_diff > s_diff) {
            q = -r - s;
        } else if (r_diff > s_diff) {
            r = -q - s;
        } else {
            s = -q - r;
        }

        return new Hex(q, r, s);
    }
}
