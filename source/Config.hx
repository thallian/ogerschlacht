package;

class Config {

    public var mapWidth(default, null): Int;
    public var mapHeight(default, null): Int;
    public var hexInfo(default, null): Map<String, HexInfo>;
    public var unitInfo(default, null): Map<String, UnitInfo>;
    public var playerInfo(default, null): Map<String, PlayerInfo>;

    public function new() {
        load();
    }

    public function load(): Void {
        loadMapInfo();
        loadUnitInfo();
        loadPlayerInfo();
    }

    private function loadMapInfo(): Void {
        this.hexInfo = new Map<String, HexInfo>();

        var mapjson: { width: Int, height: Int, fields: Array<Dynamic> } =
            haxe.Json.parse(openfl.Assets.getText("assets/data/map.json"));

        this.mapWidth = mapjson.width;
        this.mapHeight = mapjson.height;

        for(field in mapjson.fields ) {
            this.hexInfo.set(field.coordinates, new HexInfo(field.img));
        }
    }

    private function loadUnitInfo(): Void {
        this.unitInfo = new Map<String, UnitInfo>();

        var unitjson: Array<UnitInfo> =
            haxe.Json.parse(openfl.Assets.getText("assets/data/units.json"));

        for(unit in unitjson ) {
            var unitInfo = new UnitInfo(
                unit.id,
                unit.name,
                unit.img,
                unit.movement,
                unit.strength,
                unit.rangedStrength,
                unit.maxUnits);
            this.unitInfo.set(unit.id, unitInfo);
        }
    }

    private function loadPlayerInfo(): Void {
        this.playerInfo = new Map<String, PlayerInfo>();

        var playerjson: Array<{id: String, name: String, units: Array<String>, supply: { maxStartline: Int, start: Int, refresh: { sides: Int, mod: Int } }}> =
            haxe.Json.parse(openfl.Assets.getText("assets/data/players.json"));

        if(playerjson.length != 2) {
            throw "Need exactly two players defined";
        }
        for(player in playerjson) {
            var playerUnits = new Array<UnitInfo>();
            
            for(unitName in player.units) {
                if (this.unitInfo.get(unitName) != null) {
                    var playerUnit: UnitInfo = this.unitInfo.get(unitName);
                    playerUnits.push(playerUnit);
                }
            }

            var playerInfo = new PlayerInfo(
                player.id,
                player.name,
                playerUnits,
                player.supply.maxStartline,
                player.supply.start,
                player.supply.refresh.sides,
                player.supply.refresh.mod);

            this.playerInfo.set(player.id, playerInfo);
        }
    }
}
