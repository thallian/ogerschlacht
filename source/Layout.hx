package;

import flixel.util.FlxPoint;

class Layout {

    public var orientation(default, default): Orientation;
    public var size(default, default): FlxPoint;
    public var origin(default, default): FlxPoint;

    public function new(orientation: Orientation, size: FlxPoint, origin: FlxPoint) {
        this.orientation = orientation;
        this.size = size;
        this.origin = origin;
    }

    public function hex_to_pixel(hex: Hex): FlxPoint {
        var x: Float = (this.orientation.f0 * hex.q + this.orientation.f1 * hex.r) * this.size.x;
        var y: Float = (this.orientation.f2 * hex.q + this.orientation.f3 * hex.r) * this.size.y;

        return new FlxPoint(x + this.origin.x, y + this.origin.y);
    }

    public function pixel_to_hex(point: FlxPoint): FractionalHex {
        var pt = new FlxPoint((point.x - this.origin.x) / this.size.x, (point.y - this.origin.y) / this.size.y);

        var q: Float = this.orientation.b0 * pt.x + this.orientation.b1 * pt.y;
        var r: Float = this.orientation.b2 * pt.x + this.orientation.b3 * pt.y;

        return new FractionalHex(q, r, -q - r);
    }

    public function hex_corner_offset(corner: Int): FlxPoint {
        var angle: Float = 2.0 * Math.PI * (corner + this.orientation.start_angle) / 6;

        return new FlxPoint(this.size.x * Math.cos(angle), this.size.y * Math.sin(angle));
    }

    public function polygon_corners(hex: Hex): Array<FlxPoint> {
        var corners = new Array<FlxPoint>();
        var center: FlxPoint = hex_to_pixel(hex);

        for (i in 0...6) {
            var offset: FlxPoint = hex_corner_offset(i);
            var corner: FlxPoint = new FlxPoint(center.x + offset.x, center.y + offset.y);

            corners.push(corner);
        }

        return corners;
    }
}
