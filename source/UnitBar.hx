package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxPoint;
import flixel.util.FlxRect;
import flixel.text.FlxText;
import flixel.group.FlxSpriteGroup;

using flixel.util.FlxSpriteUtil;

class UnitBar extends FlxSpriteGroup {

    var hOffset:Float;
    var vOffset:Float;

    var background:FlxSprite;
    var selectionCanvas:FlxSprite;
    var rectMap:Map<FlxRect,Int>;
    var textUnitInfo: FlxText;

    var units:Array<UnitInfo>;
    var selectedUnit:UnitInfo;

    public function new(x:Float, y:Float, width:Int, height:Int) {
        super();

        this.width = width;
        this.height = height;
        this.hOffset = width / 125;
        this.vOffset = height / 5;

        createBackground(x, y, width, height);
        createSelection(x, y, width, height);
        createInfoText();
    }

    private function createBackground(x:Float, y:Float, width:Int, height:Int):Void {
        this.background = new FlxSprite();
        this.background.loadGraphic("assets/images/unitbar_back.jpg");
        this.background.updateFrameData();

		this.background.x = x;
        this.background.y = y;

        this.background.scale.set(width / this.background.frameWidth, height / this.background.frameHeight);
        this.background.setGraphicSize(Std.int(width / this.background.frameWidth), Std.int(height / this.background.frameHeight));
        this.background.updateHitbox();

        add(this.background);
    }

    private function createSelection(x:Float, y:Float, width:Int, height:Int):Void {
        this.selectionCanvas = new FlxSprite();
		this.selectionCanvas.makeGraphic(width, height, FlxColor.BLUE, true);
		this.selectionCanvas.x = x;
        this.selectionCanvas.y = y;

        add(this.selectionCanvas);
    }

    private function createInfoText() {
        this.textUnitInfo = new FlxText(0, this.background.y + this.background.height - vOffset, this.background.width);
		this.textUnitInfo.setFormat("assets/fonts/firasans.ttf", 16, FlxColor.WHITE, "center");
		add(this.textUnitInfo);
    }

    public function selectUnits(units:Array<UnitInfo>):Void {
        this.units = units;

        var unitFieldSize = 75;

        this.rectMap = new Map<FlxRect,Int>();

        var spacing:Float = this.background.width / units.length / 2;
        var unitOffset:Float = (this.background.width / 2) - ((spacing * units.length - spacing));

        for(i in 0...units.length) {
			var unit:UnitInfo = units[i];

			var unitField = new FlxSprite();
			unitField.loadGraphic(unit.img);

			unitField.scale.set(unitFieldSize / unitField.frameHeight, unitFieldSize / unitField.frameWidth);
			unitField.updateHitbox();

            var unitFieldX = Std.int(i * (unitField.frameWidth / 2) + unitField.frameWidth / 2 + this.background.x);
            unitFieldX = Std.int(spacing * i + spacing * units.length / 2 + spacing / 3);
            var unitFieldY = this.background.y + Std.int(unitField.height / 2);

            unitField.x = unitFieldX;
            unitField.y = unitFieldY;

			add(unitField);

            this.rectMap.set(new FlxRect(unitField.x, unitField.y, unitField.width, unitField.height), i);
		}
    }

    public function select(x:Float, y:Float):Void {
        var clickPoint = new FlxPoint(x, y);

        this.selectionCanvas.fill(FlxColor.TRANSPARENT);
        this.textUnitInfo.text = "";

        for (unitRect in this.rectMap.keys()) {
            var drawColour = FlxColor.TRANSPARENT;
            this.selectedUnit = null;

            if (unitRect.containsFlxPoint(clickPoint)) {
                this.selectionCanvas.drawRect(
                    unitRect.x - this.selectionCanvas.x,
                    unitRect.y - this.selectionCanvas.y,
                    unitRect.width,
                    unitRect.height,
                    FlxColor.TRANSPARENT,
                    { thickness: 4, color: FlxColor.RED });

                this.selectedUnit = this.units[rectMap.get(unitRect)];

                var rangedInfo:String = this.selectedUnit.rangedStrength > 0 ? " Fernkampf: " + this.selectedUnit.rangedStrength : "";
                this.textUnitInfo.text = this.selectedUnit.name + " - Bewegung: " + this.selectedUnit.movement + " Stärke: " + this.selectedUnit.strength + rangedInfo;
            }
        }
    }
}
