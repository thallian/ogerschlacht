package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxPoint;
import flixel.util.FlxColor;
import flixel.util.FlxSpriteUtil;
import haxe.Serializer;
import haxe.Unserializer;
import flixel.util.FlxMath;

using flixel.util.FlxSpriteUtil;

class HexMap {

    private var layout: Layout;
    private var linestyle: LineStyle;
    private var fillstyle: FillStyle;
    private var hexes: Array<Hex>;

    public var highlightHex(default, null): Hex;

    public var width(default, null): Float;
    public var height(default, null): Float;

    public function new(?origin: FlxPoint, ?size: FlxPoint, ?orientation, ?linestyle, ?fillstyle) {
        origin = origin == null ? new FlxPoint(FlxG.width / 2, FlxG.height / 2) : origin;
        size = size == null ? new FlxPoint(25, 25) : size;

        this.linestyle = linestyle == null ? { color: 0xD9D9D9D9, thickness: 1.75 } : linestyle;
        this.fillstyle = fillstyle == null ? { color: FlxColor.TRANSPARENT, alpha: 0.5 } : fillstyle;
        this.layout = new Layout(orientation == null ? Orientation.flat() : orientation, size, origin);
    }

    public function select(x: Float, y: Float) {
        var selectedHex: Hex = this.layout.pixel_to_hex(new FlxPoint(x, y)).to_hex();

        this.highlightHex = null;
        for(i in 0...this.hexes.length) {
            var hex: Hex = this.hexes[i];

            if (selectedHex.equals(hex)) {
                this.highlightHex = selectedHex;
                break;
            }
        }
    }

    public function parallelogram(q1: Int, q2: Int, r1: Int, r2: Int): Void {
        this.hexes = new Array<Hex>();

        for (q in q1...q2 + 1) {
            for(r in r1...r2 + 1) {
                var hex = new Hex(q, r, -q-r);
                this.hexes.push(hex);
            }
        }
    }

    public function triangle(size: Int): Void {
        this.hexes = new Array<Hex>();

        if (this.layout.orientation == Orientation.pointy()) {
            for (q in 0...size + 1) {
                for (r in 0...size + 1 - q) {
                    var hex = new Hex(q, r, -q-r);
                    this.hexes.push(hex);
                }
            }
        } else {
            for (q in 0...size + 1) {
                for (r in size - q...size) {
                    var hex = new Hex(q, r, -q-r);
                    this.hexes.push(hex);
                }
            }
        }
    }

    public function hexagon(radius: Int): Void {
        this.hexes = new Array<Hex>();

        for(q in -radius...radius + 1) {
            var r1: Int = Std.int(Math.max(-radius, -q - radius));
            var r2: Int = Std.int(Math.min(radius, -q + radius));

            for(r in r1...r2 + 1) {
                var hex = new Hex(q, r, -q-r);
                this.hexes.push(hex);
            }
        }
    }

    public function rectangle(width: Int, height: Int): Void {
        this.hexes = new Array<Hex>();

        if (this.layout.orientation == Orientation.pointy()) {
            for(r in 0...height) {
                var r_offset: Int = Math.floor(r / 2);

                for (q in -r_offset...width - r_offset) {
                    var hex = new Hex(q, r, -q-r);
                    this.hexes.push(hex);
                }
            }
        } else {
            for(q in 0...width) {
                var q_offset: Int = Math.floor(q / 2);

                for (r in -q_offset...height - q_offset) {
                    var hex = new Hex(q, r, -q-r);
                    this.hexes.push(hex);
                }
            }
        }
    }

    public function drawSelection(canvas: FlxSprite): Void {
        if (this.highlightHex != null) {
            var corners: Array<FlxPoint> = this.layout.polygon_corners(this.highlightHex);
            canvas.drawPolygon(corners, FlxColor.TRANSPARENT, { thickness: 1.5, color: FlxColor.RED }, { color: FlxColor.TRANSPARENT });
        }
    }

    public function clear(canvas: FlxSprite): Void {
        canvas.fill(FlxColor.TRANSPARENT);
    }

    public function draw(canvas: FlxSprite): Void {
        var maxX: Float = 0;
        var maxY: Float = 0;

        for(i in 0...this.hexes.length ) {
			var hex: Hex = this.hexes[i];
			var corners: Array<FlxPoint> = this.layout.polygon_corners(hex);

            var hexMaxX: Float = 0;
            var hexMaxY: Float = 0;
            for(j in 0...corners.length ) {
                if (hexMaxX < corners[j].x) {
                    hexMaxX = corners[j].x;
                }

                if (hexMaxY < corners[j].y) {
                    hexMaxY = corners[j].y;
                }
            }

            if (maxX < hexMaxX) {
                maxX = hexMaxX;
            }

            if (maxY < hexMaxY) {
                maxY = hexMaxY;
            }

			canvas.drawPolygon(corners, FlxColor.TRANSPARENT, this.linestyle, this.fillstyle);
		}

        this.width = maxX;
        this.height = maxY;
    }

    public function drawTextures(canvas: FlxSprite): Void {
        for(i in 0...this.hexes.length ) {
            var hex: Hex = this.hexes[i];

            if (Reg.config.hexInfo.exists(hex.key())) {
    			var corners: Array<FlxPoint> = this.layout.polygon_corners(hex);

                var hexTex = new FlxSprite();
                hexTex.loadGraphic(Reg.config.hexInfo.get(hex.key()).img);

                var widthPoint1: FlxPoint = corners[0];
                var widthPoint2: FlxPoint = corners[3];

                var heightPoint1: FlxPoint = corners[1];
                var heightPoint2: FlxPoint = corners[5];

                var hexWidth: Float = FlxMath.getDistance(widthPoint1, widthPoint2);
                var hexHeight: Float = FlxMath.getDistance(heightPoint1, heightPoint2);

                var widthRatio: Float =  hexWidth / hexTex.frameWidth;
                var heightRatio: Float =  hexHeight / hexTex.frameHeight;

                hexTex.scale.set(widthRatio, heightRatio);
                hexTex.updateHitbox();

                canvas.stamp(hexTex, Std.int(corners[3].x - hexTex.offset.x), Std.int(corners[4].y - hexTex.offset.y));
            }
        }
    }
}
