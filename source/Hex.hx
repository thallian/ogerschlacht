package;

enum HexDirection {
    Zero;
    One;
    Two;
    Three;
    Four;
    Five;
}

class Hex {

    public var q(default, null): Int;
    public var r(default, null): Int;
    public var s(default, null): Int;

    private static var directions = [
        new Hex(1, 0, -1),
        new Hex(1, -1, 0),
        new Hex(0, -1, 1),
        new Hex(-1, 0, 1),
        new Hex(-1, 1, 0),
        new Hex(0, 1, -1)
    ];

    public function new(q: Int, r: Int, ?s: Int) {
        this.q = q;
        this.r = r;
        this.s = s == null ? (-q -r) : s;
    }

    public function linedraw(b: Hex): Array<Hex> {
        var N: Int = distance(b);
        var results = new Array<Hex>();
        var step: Float = 1.0 / Math.max(N, 1);

        for (i in 0...N + 1) {
            var lineHex: Hex = lerp(b, step * i).to_hex();
            results.push(lineHex);
        }

        return results;
    }

    // linear interpolation
    public function lerp(b: Hex, t: Float): FractionalHex {
        return new FractionalHex(this.q + (b.q - this.q) * t,
                             this.r + (b.r - this.r) * t,
                             this.s + (b.s - this.s) * t);
    }

    public function neighbour(dir: HexDirection): Hex {
        return add(direction(dir));
    }

    public function direction(direction: HexDirection): Hex {
        var enumIndex: Int = Type.enumIndex(direction);
        return Hex.directions[enumIndex];
    }

    public function length(): Int {
        return cast((Math.abs(this.q) + Math.abs(this.r) + Math.abs(this.s)) / 2, Int);
    }

    public function distance(b: Hex): Int {
        return subtract(b).length();
    }

    public function equals(b: Hex): Bool {
        return this.q == b.q && this.r == b.r && this.s == b.s;
    }

    public function notEquals(b: Hex): Bool {
        return !this.equals(b);
    }

    public function add(b: Hex): Hex {
        return new Hex(this.q + b.q, this.r + b.r, this.s + b.s);
    }

    public function subtract(b: Hex): Hex {
        return new Hex(this.q - b.q, this.r - b.r, this.s - b.s);
    }

    public function multiply(b: Hex): Hex {
        return new Hex(this.q * b.q, this.r * b.r, this.s * b.s);
    }

    public function toString() {
        return "Hex<" + this.q + "," + this.r + "," +  this.s + ">";
    }

    public function key() {
        return this.q + "," + this.r + "," + this.s;
    }
}
