package;

class PlayerInfo {

    public var id (default, null): String;
    public var name (default, null): String;
    public var units (default, null): Array<UnitInfo>;
    public var supplyMaxStartline (default, null): Int;
    public var supplyStart (default, null): Int;
    public var supplyRefreshSides (default, null): Int;
    public var supplyRefreshMod (default, null): Int;

    public function new(
            id: String,
            name: String,
            units: Array<UnitInfo>,
            supplyMaxStartline: Int,
            supplyStart: Int,
            supplyRefreshSides: Int,
            supplyRefreshMod: Int) {
        this.id = id;
        this.name = name;
        this.units = units;
        this.supplyMaxStartline = supplyMaxStartline;
        this.supplyStart = supplyStart;
        this.supplyRefreshSides = supplyRefreshSides;
        this.supplyRefreshMod = supplyRefreshMod;
    }
}
