package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.util.FlxColor;

using flixel.util.FlxSpriteUtil;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class PlayState extends FlxState
{
	var gui: FlxSpriteGroup;
	var mapCanvas: FlxSprite;
	var overlayCanvas: FlxSprite;
	var coordinates: FlxText;
	var textTitle: FlxText;
	var hexmap: HexMap;

	var unitBar:UnitBar;

	static var CAMSPEED = 15;

	var topOffset: Int;
	var bottomOffset: Int;
	var leftOffset: Int;
	var rightOffset: Int;

	/**
	 * Function that is called up when to state is created to set it up.
	 */
	override public function create():Void
	{
		this.topOffset = Std.int(FlxG.height / 40);
		this.bottomOffset = Std.int(FlxG.height / 5);
		this.leftOffset = 0;
		this.rightOffset = 0;

		var mapBackCanvas = new FlxSprite();
		mapBackCanvas.makeGraphic(
			FlxG.width - this.leftOffset - this.rightOffset,
			FlxG.height - this.topOffset - this.bottomOffset,
			FlxColor.BLACK,
			true);
		mapBackCanvas.x = this.leftOffset;
		mapBackCanvas.y = this.topOffset;
		add(mapBackCanvas);

		this.mapCanvas = new FlxSprite();
		this.mapCanvas.makeGraphic(FlxG.width * 2, FlxG.height * 2, FlxColor.BLACK, true);
		this.mapCanvas.x = this.leftOffset;
		this.mapCanvas.y = this.topOffset;
		add(this.mapCanvas);

		this.overlayCanvas = new FlxSprite();
		this.overlayCanvas.makeGraphic(FlxG.width * 2, FlxG.height * 2, FlxColor.BLACK, true);
		this.overlayCanvas.x = this.leftOffset;
		this.overlayCanvas.y = this.topOffset;
		add(this.overlayCanvas);

		this.hexmap = new HexMap(new FlxPoint(30,30), new FlxPoint(28,28));
		this.hexmap.rectangle(Reg.config.mapWidth, Reg.config.mapHeight);
		this.hexmap.drawTextures(this.mapCanvas);
		this.hexmap.draw(this.mapCanvas);
		this.hexmap.drawSelection(this.mapCanvas);

		this.gui = new FlxSpriteGroup();
		add(this.gui);

		var topBar = new FlxSprite();
		topBar.makeGraphic(FlxG.width, this.topOffset, FlxColor.GREEN, true);
		this.gui.add(topBar);

		this.coordinates = new FlxText(1,1);
		this.coordinates.setFormat("assets/fonts/firasans.ttf", 12, FlxColor.WHITE, "center");
		this.gui.add(this.coordinates);

		this.textTitle = new FlxText(0 ,1, FlxG.width);
		this.textTitle.setFormat("assets/fonts/firasans.ttf", 12, FlxColor.WHITE, "center");
		this.gui.add(this.textTitle);

		this.unitBar = new UnitBar(0, FlxG.height - this.bottomOffset, FlxG.width, this.bottomOffset);
		this.gui.add(this.unitBar);

		phaseSetup(Reg.config.playerInfo.get("mittelreich"));

		super.create();
	}

	private function phaseSetup(player: PlayerInfo) {
		this.textTitle.text = "Setup for " + player.name;
		this.unitBar.selectUnits(player.units);
	}

	/**
	 * Function that is called when this state is destroyed - you might want to
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		var dirty = false;

		if (FlxG.keys.anyPressed(["UP", "W"]) &&
				this.mapCanvas.y + CAMSPEED <= this.topOffset) {
			this.mapCanvas.y += CAMSPEED;
			dirty = true;
		}
		if (FlxG.keys.anyPressed(["DOWN", "S"]) &&
				this.mapCanvas.y + this.hexmap.height - CAMSPEED >= FlxG.height - this.topOffset - this.bottomOffset) {
			this.mapCanvas.y -= CAMSPEED;
			dirty = true;
		}
		if (FlxG.keys.anyPressed(["LEFT", "A"]) &&
				this.mapCanvas.x + CAMSPEED <= this.leftOffset) {
			this.mapCanvas.x += CAMSPEED;
			dirty = true;
		}
		if (FlxG.keys.anyPressed(["RIGHT", "D"]) &&
				this.mapCanvas.x + this.hexmap.width - CAMSPEED >= FlxG.width - this.rightOffset) {
			this.mapCanvas.x -= CAMSPEED;
			dirty = true;
		}

		this.overlayCanvas.x = this.mapCanvas.x;
		this.overlayCanvas.y = this.mapCanvas.y;

		this.hexmap.select(
			FlxG.mouse.screenX - this.mapCanvas.x,
			FlxG.mouse.screenY - this.mapCanvas.y);

		if (FlxG.mouse.justReleased) {
			this.unitBar.select(FlxG.mouse.screenX, FlxG.mouse.screenY);
		}

		this.hexmap.clear(this.overlayCanvas);
		this.hexmap.drawSelection(this.overlayCanvas);

		this.coordinates.text = this.hexmap.highlightHex == null ? "" : this.hexmap.highlightHex.toString();

		super.update();
	}
}
