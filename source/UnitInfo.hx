package;

class UnitInfo {

    public var id (default, null): String;
    public var name (default, null): String;
    public var img (default, null): String;
    public var movement (default, null): Int;
    public var strength (default, null): Int;
    public var rangedStrength (default, null): Int;
    public var maxUnits (default, null): Int;

    public function new(
            id: String,
            name: String,
            img: String,
            movement: Int,
            strength: Int,
            rangedStrength: Int,
            maxUnits: Int) {
        this.id = id;
        this.name = name;
        this.img = img;
        this.movement = movement;
        this.strength = strength;
        this.rangedStrength = rangedStrength;
        this.maxUnits = maxUnits;
    }
}
