# Battle of the Ogres
This is a small project to learn a little bit about game development on one hand and about [Haxe](http://haxe.org/) on the other.

It tries to implement the battle of the ogres as it is described in the adventure [More than 1000 Ogres](http://www.wiki-aventurica.de/wiki/Mehr_als_1000_Oger) from the german pen & paper rpg [The Dark Eye](https://en.wikipedia.org/wiki/The_Dark_Eye).

I spent countless hours playing both the rpg and the ogre boardgame (I still do) so I wanted to try my hands on an implementation. So far I am working on the setup phase.

A very big thanks to Red Blob Games and their fantastic introduction into [hexagonal grids](http://www.redblobgames.com/grids/hexagons/)!

![Screenshot](screenshot.png)

# Prerequisites
- [Haxe](http://haxe.org/)
- [HaxeFlixel](http://haxeflixel.com/)

# Usage
```
lime test neko
```

Move the map with WASD or the arrow keys. Click on the different unit cards to get details.

# License
The project is licensed under the [MPL2.0](https://www.mozilla.org/MPL/2.0/index.txt).

